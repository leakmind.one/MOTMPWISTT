A site that allows you to post any message on the home page of the site.

All messages are published in a telegram group.

**Config file**: `/data/config.php`

**System requirements**: PHP 5.6+ 
