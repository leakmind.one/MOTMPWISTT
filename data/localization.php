<?php
require($DIR_SITE . "config.php");
@$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
if (empty($lang)) {
$lang = "en";
}

$header_description_loc = array (
    "en" => "$NAME_SITE - a site that allows you to post any message on the home page of the sit.e",
    "ru" => "$NAME_SITE - сайт, который позволяет опубликовать любое сообщение на главной странице сайта.",
    "uk" => "$NAME_SITE - сайт, який дозволяє опублікувати будь-яке повідомлення на головній сторінці сайті.",
);
$header_keywords_loc = array (
    "en" => "$NAME_SITE, blog, post, post, free, free blog, anonymous blog, publish, appeal",
    "ru" => "$NAME_SITE, блог, сообщение, запостить, бесплатно, блог, анонимный блог, опубликовать, обращение",
    "uk" => "$NAME_SITE, блог, повідомлення, запостити, безкоштовно, блог, анонімний блог, опублікувати, звернення",
);
$header_locale_loc = array (
    "en" => "en_EN",
    "ru" => "ru_RU",
    "uk" => "uk_UK",
);
$header_og_title_loc = array (
    "en" => "$NAME_SITE - post a message on the home page of the site",
    "ru" => "$NAME_SITE - разместите сообщение на главной странице сайте",
    "uk" => "$NAME_SITE - розмістіть повідомлення на головній сторінці сайті",
);
$button_not_loc = array (
    "en" => "NO",
    "ru" => "НЕТ",
    "uk" => "НІ",
);
$button_yes_loc = array (
    "en" => "YES",
    "ru" => "ДА",
    "uk" => "ТАК",
);
$menu_home_loc = array(
    "en" => "Home",
    "ru" => "Главная",
    "uk" => "Головна",
);


$menu_feedback_loc = array(
    "en" => "Feedback",
    "ru" => "Обратная связь",
    "uk" => "Зворотній зв'язок",
);


$name_loc = array(
    "en" => "Name",
    "ru" => "Имя",
	"uk" => "Ім'я",
);

$button_loc = array(
    "en" => "Post message",
    "ru" => "Опубликовать сообщение",
	"uk" => "Опублікувати повідомлення",
);


$message_loc = array(
    "en" => "Message",
    "ru" => "Сообщение",
	"uk" => "Повідомлення",
);

$error_404_loc = array(
    "en" => '<h1 class="display-3">Error 404</h1><p class="lead">Page not found. This can happen for the following reasons:
<ul class="lead">- page has been deleted or renamed;</ul>
<ul class="lead">- page never existed;</ul>
<ul class="lead">- you had the wrong link.</ul></p>
<p class="lead"><b>What should I do in this case?</b>
<ul class="lead">- Go to <a href="/">Home page</a>.</ul>
<ul class="lead">- Go back by pressing the browser\'s back.</ul>
<ul class="lead">- Check the spelling of the page address (URL).</ul>
<ul class="lead">- Visit the main sections of the site.</ul></p>
<p class="lead">If you are sure that this is a misunderstanding and there should definitely be something useful, please contact us: ' . $email_site .'</p>',
    "ru" => '<h1 class="display-3">Error 404</h1><p class="lead">Страница не найдена. Это могло произойти по следующим причинам:
<ul class="lead">- страница была удалена или переименована;</ul>
<ul class="lead">- страница никогда не существовала;</ul>
<ul class="lead">- у Вас была неверная ссылка.</ul></p>
<p class="lead"><b>Что делать в этом случае?</b>
<ul class="lead">- Перейти на <a href="/">Главную страницу</a>.</ul>
<ul class="lead">- Вернуться назад при помощи кнопки браузера "вернуться".</ul>
<ul class="lead">- Проверьте правильность написания адреса страницы (URL).</ul>
<ul class="lead">- Посетить основные разделы сайта.</ul></p>
<p class="lead">Если вы уверены, что это недоразумение и там обязательно должно быть что-то полезное, то, пожалуйста, свяжитесь с нами: ' . $email_site .'</p>
',
    "uk" => '<h1 class="display-3">Error 404</h1><p class="lead">Сторінку не знайдено. Це могло статися з таких причин:
<ul class="lead">- сторінка була видалена або перейменована;</ul>
<ul class="lead">- сторінка ніколи не існувала;</ul>
<ul class="lead">- у Вас було невірне посилання.</ul></p>
<p class="lead"><b>Що робити в цьому випадку?</b>
<ul class="lead">- Перейти на <a href="/">Головну сторінку</a>.</ul>
<ul class="lead">- Поверніться назад за допомогою кнопки браузера "назад".</ul>
<ul class="lead">- Перевірте правильність написання адреси сторінки (URL).</ul>
<ul class="lead">- Відвідати основні розділи сайту.</ul></p>
<p class="lead">Якщо ви впевнені, що це якесь непорозуміння і тут обов\'язково має бути щось корисне, то, будь ласка, зв\'яжіться з нами: ' . $email_site .'</p>',
);


$error_payment_loc = array(
    "en" => "<br><p>Are you sure you want to publish what you wrote?</p>",
    "ru" => "<br><p>Вы точно уверены что хотите опубликовать то, что написали?</p>",
    "uk" => "<br><p>Ви точно впевнені що хочете опублікувати те, що написали?</p>",
);

$header_name_loc = array(
    "en" => "Post any message on the site",
    "ru" => "Разместите любое сообщение на сайте",
    "uk" => "Додайте будь-яке повідомлення на сайті",
);

$error_name_loc = array(
    "en" => "Name is not enter!",
    "ru" => "Имя не введено!",
	"uk" => "Ім'я не введено!",
);

$error_message_loc = array(
    "en" => "Message is not enter!",
    "ru" => "Сообщение не введено!",
	"uk" => "Повідомлення не введено!",
);

$error_letters_loc = array(
	"en" => "The name should contain only Latin or Russian letters!",
	"ru" => "Имя должно содержать только латинские или русские буквы!",
	"uk" => "Ім'я повинно містити лише латинські або російські літери!",
);

$footer_text_loc = array(
    "en" => "All messages are duplicated in the Telegram group: <a href='https://t.me/$ID_GROUP'>$NAME_SITE </a>",
    "ru" => "Все сообщения дублируются в группу Telegram: <a href='https://t.me/$ID_GROUP'>$NAME_SITE </a>",
    "uk" => "Всі повідомлення дублюються в групу Telegram: <a href='https://t.me/$ID_GROUP'>$NAME_SITE </a>",
);

$feedback_loc = array(
    "en" => '<h1 class="display-3">Feedback</h1><p class="lead">E-mail: <a href="mailto:' . $email_site .'">' . $email_site .'</a></p>',
    "ru" => '<h1 class="display-3">Контакты</h1><p class="lead">E-mail: <a href="mailto:' . $email_site .'">' . $email_site .'</a></p>',
    "uk" => '<h1 class="display-3">Контакти</h1><p class="lead">E-mail: <a href="mailto:' . $email_site .'">' . $email_site .'</a></p>',
);

?>
