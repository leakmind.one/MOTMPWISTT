<?php require($DIR_SITE . "config.php"); ?>
<html lang="en"><head>
    <meta charset="utf-8">
        <title><?php echo $button_loc[$lang]; ?> | <?php echo $NAME_SITE; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="<?php echo $header_description_loc[$lang] ?>"/>
		<meta name="keywords" content="<?php echo $header_keywords_loc[$lang] ?>"/>
		<meta property="og:locale" content="<?php echo $header_locale_loc[$lang] ?>" />
		<meta property="og:site_name" content="<?php echo $NAME_SITE; ?>" />
		<meta property="og:title" content="<?php echo $header_og_title_loc[$lang] ?>" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="<?php echo $URL_SITE; ?>" />
		<meta property="og:image" content="./img/logo.png" />
<link rel="icon" type="image/png" href="./img/logo.png" sizes="16x16">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" crossorigin="anonymous">
<script src="https://www.google.com/recaptcha/api.js"></script>
  </head>
<body><div class="container">
  <nav class="navbar navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="/">
      <img src="./img/logo.png" alt="" width="30" height="28" class="d-inline-block align-top">
      <?php echo $NAME_SITE; ?>
    </a>
  </div>
</nav>
