<?php 
require($DIR_SITE . "config.php");
echo '<footer>

<div class="card">
  <div class="card-body">

<p class="card-text">' . @$header_description_loc[$lang]  . '    </p>
 <p class="card-text"><small>' . @$footer_text_loc[$lang] . '</small></p>
<p class="text-right"> <small class="text-muted">' . date("Y") . ' | <a href="'. $URL_SITE .'">' . $NAME_SITE . '</a></small> </p>

  </div>
</div>
</footer>';

echo '</div><script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" crossorigin="anonymous"></script><script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" crossorigin="anonymous"></script><script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" crossorigin="anonymous"></script></body>';

?>
