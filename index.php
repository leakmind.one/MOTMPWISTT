<?php
require("./data/config.php");
require("./data/localization.php");
require("./data/header.php");

echo '<div class="shadow-lg p-3 mb-5 bg-white rounded">';
$text_name = file_get_contents("./storage/name.txt");
$text_name = htmlspecialchars($text_name);
echo '<h1 class="display-4">' . $text_name . '</h1>';
$text_message = file_get_contents("./storage/message.txt");
$text_message = htmlspecialchars($text_message);
echo '<p class="lead">' .$text_message . '</p>';
echo '</div>';
echo "<p class='lead text-center'>" . $header_name_loc[$lang] . "</p>";
echo '<form method="post">
<input type="text" class="form-control" name="name" placeholder="' . $name_loc[$lang] . '"><br>
<textarea type="text" class="form-control" name="message" placeholder="' . $message_loc[$lang] . '"></textarea><br>
<div class="g-recaptcha" data-sitekey="' . $public_key . '" style="";></div>
<input type="submit" class="btn btn-primary" name="send" value="' . $button_loc[$lang] . '">
</form>';
if (isset($_REQUEST['send'])) {
$arrays = array("\\", "#", "'", "\"", "<", "%", "/", ">", "*", "$", "_", ":", "?", "-", "(", ")", ";", "\@");
$name = str_replace($arrays, "", trim(mb_substr($_POST['name'], 0, 40)));
$message = str_replace($arrays, "", trim(mb_substr($_POST['message'], 0, 512)));
if (empty($_POST['name'])) {
echo "<div class='alert alert-info'>" . $error_name_loc[$lang] . "</div>";
} else {
	if (empty($_POST['message'])){
		echo "<div class='alert alert-info'>" . $error_message_loc[$lang] . "</div>";
	} else {
		if (preg_match("/[^(\w)|(\x7F-\xFF)|(\s)]/",$name)){
			echo "<div class='alert alert-info'>" . $error_letters_loc[$lang] . "</div>";
			} else { 
if (isset($_POST['g-recaptcha-response']) && $_POST['g-recaptcha-response']) {

  $ip = $_SERVER['REMOTE_ADDR'];
  $response = $_POST['g-recaptcha-response'];
  $rsp = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=$response&remoteip=$ip");
  $arr = json_decode($rsp, TRUE);
  if ($arr['success']) {

    function generatePass($length = 16)
    {
        $chars = 'qazwsxedcrfvtgbyhnujmikolpQAZWSXEDCRFVTGBYHNUJMIKOLP1234567890-_';
        $numChars = strlen($chars);
        $string = '';
    for ($i = 0; $i < $length; $i++)
        {
        $string .= substr($chars, rand(1, $numChars) - 1, 1);
        }
    return $string;
    }
$micro = generatePass(16);
$number = date("YmdHis");
$order_id = $number.$micro;
$ips = $_SERVER["REMOTE_ADDR"];
    function generatePassword($length = 16){
    	$chars = 'qazwsxedcrfvtgbyhnujmikolpQAZWSXEDCRFVTGBYHNUJMIKOLP1234567890';
        $numChars = strlen($chars);
        $string = '';
    	for ($i = 0; $i < $length; $i++){
        	$string .= substr($chars, rand(1, $numChars) - 1, 1);
        }
    return $string;
    }
	$name_file_create = generatePassword(16);
	$file_create = "./" . $name_file_create . ".php";
  $red_link = substr($file_create, 1);
	if (!file_exists($file_create)) {
    	$fp = fopen($file_create, "w");
		$content_file_client = '<?php require("./data/localization.php");
require("./data/header.php");
echo $error_payment_loc[$lang];
echo \'<form method="post">
<input type="submit" class="btn btn-success" name="not" value="\' . $button_not_loc[$lang] . \'">
<input type="submit" class="btn btn-warning" name="yes" value="\' . $button_yes_loc[$lang] . \'">
</form>\';
if (isset($_REQUEST[\'not\']))  {
unlink(__FILE__);
header("Location: /");
} 
elseif (isset($_REQUEST[\'yes\']))  {
$name_content = "' . $name . '";
$message_content = "' . $message . '";
file_put_contents("./storage/name.txt", "");
file_put_contents("./storage/message.txt", "");
$filed_name = "./storage/name.txt";
$content_name = $name_content;
file_put_contents($filed_name, $content_name);
$filed_message = "./storage/message.txt";
$content_message = $message_content ;
file_put_contents($filed_message, $content_message);
$botToken="' . $TOKEN_BOT . '";
$telegram_message = "' . $name . '

' . $message . '";
$website="https://api.telegram.org/bot".$botToken;
$chatId=\'' . $ID_GROUP . '\';
$params=[
      "chat_id"=>$chatId, 
      "text"=>$telegram_message,
];
$ch = curl_init($website . "/sendMessage");
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, ($params));
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
$result = curl_exec($ch);
curl_close($ch);
unlink(__FILE__);
header("Location: /");
exit; 
}
require("./data/footer.php"); ?>';
		    fwrite($fp, $content_file_client);
		    fclose($fp);
			}

header("Location: $URL_SITE" . $red_link);


} else {
echo "error data reCAPTCHA ";
}


}}}}}
require("./data/footer.php");
$dir = "./";
$files = scandir( $dir );
$time = time();
$life_file = 86400;
$time = $time - $life_file;
$ignore = array('.', '..', basename(__FILE__), 'index.php', '.htaccess', 'about.php', 'feedback.php', 'data', 'img', 'storage', '404.php');
foreach( $files as $file )
{
    if( !in_array($file, $ignore) )
    {
    $file = $dir.$file;
    $filemtime = filemtime( $file );
    if( $filemtime <= $time )
    {
    unlink( $file );
}}}
?>
